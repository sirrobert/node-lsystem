"use strict";

const LSystem = require('../index.js');
const expect  = require('unexpected');

describe("class LSystem", function () {

  describe("#addToken(params)", function () {
    it("should add previously unknown symbols", function () {
      var system = new LSystem();
      system.axiom = 'AB';
      system.addToken({glyph: 'A', expansion: 'AB'});

      var error = false;
      system.on("[LSystem.expand]error:unknown-token", data => { error = true; });

      var expanded = system.expand();  
      expect(expanded, "to be", "ABB");
      expect(error, "to be true");

      system.addToken({glyph: 'B', expansion: 'C'});
      error = false;
      expect(error, "to be false");
      expanded = system.expand();  
      expect(expanded, "to be", "ABC");
      expect(error, "to be false");
    });
  });

  describe("#removeToken(glyph)", function () {
    it("should remove recognized symbols", function () {
      var error = false;
      var system = new LSystem();
      system.axiom = 'AB';
      system.addToken({glyph: 'A', expansion: 'AB'});
      system.addToken({glyph: 'B', expansion: 'C'});
      system.on("[LSystem.expand]error:unknown-token", data => { error = true; });

      expect(error, "to be false");
      var expanded = system.expand();  
      expect(expanded, "to be", "ABC");
      expect(error, "to be false");

      error = false;
      system.removeToken('B');
      expanded = system.expand();
      expect(error, "to be true");
      expect(expanded, "to be", "ABB");
    });
  });

  describe("#expand()", function () {
    var system = new LSystem();
    system.axiom = 'ABC';
    system
    .addToken({glyph: 'A', expansion: 'AB'})
    .addToken({glyph: 'B', expansion: 'C'})
    .addToken({glyph: 'C', isTerminal: true});

    it("should perform single-step expansion of any axiom", function () {
      let expanded = '';
      // Now we'll manually expand it and check that it gets expanded well.
      expect(system.axiom, "to be", "ABC");
      expanded = system.expand();
      expect(expanded, "to be", "ABCC");
      expanded = system.expand(expanded);
      expect(expanded, "to be", "ABCCC");
      expanded = system.expand(expanded);
      expect(expanded, "to be", "ABCCCC");
    });

    it("should balk at unknown glyphs", function () {
      system.axiom = 'ABFA';
      var error = false;
      system.on("[LSystem.expand]error:unknown-token", data => {
        error = true;
      });

      var expanded = system.expand();
      expect(error, "to be true");
      // make sure that the unknown token is preserved.
      expect(expanded, "to be", "ABCFAB");
    });
  });

  describe("#expand()", function () {
    var system = new LSystem();
    system.axiom = 'A';

    system
    .addToken({glyph: 'A', expansion: 'AB'})
    .addToken({glyph: 'B', expansion: 'C'})
    .addToken({glyph: 'C', isTerminal: true});

    it("should perform single-step expansion of any axiom", function () {
      expect(system.resolve(0), "to be", "A");
      expect(system.resolve(1), "to be", "AB");
      expect(system.resolve(2), "to be", "ABC");
      expect(system.resolve(3), "to be", "ABCC");
      expect(system.resolve(4), "to be", "ABCCC");
    });

    it("should resolve to the preset depth", function () {

      system.depth = 0;
      expect(system.resolve(), "to be", "A");
      system.depth = 1;
      expect(system.resolve(), "to be", "AB");
      system.depth = 2;
      expect(system.resolve(), "to be", "ABC");
      system.depth = 3;
      expect(system.resolve(), "to be", "ABCC");
      system.depth = 4;
      expect(system.resolve(), "to be", "ABCCC");
    });
  });

  describe("#toJSONObject", function () {
    var error = false;
    var system = new LSystem();
    system.axiom = 'AB';
    system.depth = 4;
    system.addToken({glyph: 'A', expansion: 'AB'});
    system.addToken({glyph: 'B', expansion: 'C'});
    system.addToken({glyph: 'B', isTerminal: true});

    it("should serialize to a JSON object correctly", function () {
      var obj = system.toJSONObject();
      expect(obj.axiom   , "to be"    , "AB");
      expect(obj.depth   , "to equal" , 4);
      expect(obj.symbols , "to have keys", "A", "B", "[", "]");

      expect(obj.symbols['A'], "to have keys", "glyph", "isTerminal", "expansion");
      expect(obj.symbols['A'].glyph, "to be", "A");
      expect(obj.symbols['A'].expansion, "to be", "AB");
      expect(obj.symbols['A'].isTerminal, "to be false");
    });
  });

  describe("#fromJSONObject(obj)", function () {
    var system = new LSystem();
    system.fromJSONObject({
      axiom: 'A',
      depth: 3,
      symbols: {
        'A': { glyph: 'A', expansion: 'AB'  },
        'B': { glyph: 'B', expansion: 'C'   },
        'C': { glyph: 'C', isTerminal: true },
      }
    });

    expect(system.resolve(), "to be", "ABCC");

  });

  describe("#toJSON", function () {
    var error = false;
    var system = new LSystem();
    system.axiom = 'AB';
    system.depth = 4;
    system.addToken({glyph: 'A', expansion: 'AB'});
    system.addToken({glyph: 'B', expansion: 'C'});
    system.addToken({glyph: 'C', isTerminal: true});

    it("should serialize to a JSON string", function () {
      var json = system.toJSON();
      expect(json, "to be", '{"axiom":"AB","depth":4,"symbols":{"[":{"glyph":"[","expansion":"[","isTerminal":true},"]":{"glyph":"]","expansion":"]","isTerminal":true},"A":{"glyph":"A","expansion":"AB","isTerminal":false},"B":{"glyph":"B","expansion":"C","isTerminal":false},"C":{"glyph":"C","expansion":"C","isTerminal":true}}}');
    });
  });

  describe("#fromJSON(str)", function () {
    it("should create a good LSystem object from a JSON string", function () {
      var str = '{"axiom":"AB","depth":4,"symbols":{"+":{"glyph":"+","expansion":"+","isTerminal":true},"-":{"glyph":"-","expansion":"-","isTerminal":true},"[":{"glyph":"[","expansion":"[","isTerminal":true},"]":{"glyph":"]","expansion":"]","isTerminal":true},"A":{"glyph":"A","expansion":"AB","isTerminal":false},"B":{"glyph":"B","expansion":"C","isTerminal":false},"C":{"glyph":"C","expansion":"C","isTerminal":true}}}';

      var system = new LSystem();
      system.fromJSON(str);
      expect(system.resolve(), "to be", "ABCCCC");
    });

  });

});


