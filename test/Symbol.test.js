"use strict";

const Token = require('../src/Token.js');
const expect = require('unexpected');

describe("class Token", function () {

  describe("#isTerminal/#isVariable", function () {

    it("should be antonyms and default to variable", function () {
      var symbol = new Token({
        glyph: 'A',
        expansion: 'ABA'
      });
      
      expect(symbol.glyph      , "to be"       , "A"  );
      expect(symbol.expansion  , "to be"       , "ABA");
      expect(symbol.isTerminal , "to be false"        );
      expect(symbol.isVariable , "to be true"         );

    });

    it("should be antonyms and be terminal without expansion", function () {
      var symbol = new Token({ glyph: 'F' });
      expect(symbol.glyph     , "to be" , "F");
      expect(symbol.expansion , "to be" , "F");
      expect(symbol.isTerminal, "to be true" );
      expect(symbol.isVariable, "to be false");
    });
  });

  describe("set isTerminal(bool)", function () {
    it("should be antonyms and  terminal without expansion", function () {
      var symbol = new Token({ glyph: 'F', expansion: "G" });

      expect(symbol.glyph     , "to be" , "F" );
      expect(symbol.expansion , "to be" , "G" );
      expect(symbol.isTerminal, "to be false" );
      expect(symbol.isVariable, "to be true"  );

      symbol.isTerminal = true;

      expect(symbol.glyph     , "to be" , "F");
      expect(symbol.expansion , "to be" , "F");
      expect(symbol.isTerminal, "to be true" );
      expect(symbol.isVariable, "to be false");
    });
  });
});
