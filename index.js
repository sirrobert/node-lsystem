"use strict";

// Because of the way replacements are made (serially), we can "stream" out
// the resolveed string in chunks rather than needing to keep a very long
// string in memory.  I haven't implemented this yet, but it could be cool.

// An L-system is context-free if each production rule refers only to an
// individual symbol and not to its neighbours. Context-free L-systems are
// thus specified by a context-free grammar. If a rule depends not only on a
// single symbol but also on its neighbours, it is termed a
// context-sensitive L-system.  This module implementes context-free
// L-Systems only at this point.

// If there is exactly one production for each symbol, then the L-system is
// said to be deterministic (a deterministic context-free L-system is
// popularly called a D0L system). If there are several, and each is chosen
// with a certain probability during each iteration, then it is a stochastic
// L-system.  This module implements deterministic L-Systems only, at this
// point.

// I haven't yet resolved on how to deal with interpreters.  I think I want
// to treat them as a separate system, so you can have an L-System that
// produces axiom expansion, and one that resolves it into, say, images,
// sound, code, etc.


const Renderer     = require('./src/Renderer.js');
const Token       = require('./src/Token.js');
const EventEmitter = require('events');

class LSystem extends EventEmitter {

  constructor (params = {}) {
    super();

    this.axiom = params.axiom || '';
    this.symbols = {
      // store current interpreter information (like position, color, etc.)
      "[": new Token({glyph: "[", isTerminal: true}),
      // Restore stored interpreter information (like position, color, etc.)
      "]": new Token({glyph: "]", isTerminal: true}),
    };

    this.depth = params.depth || 1;

    Object.assign(this.symbols, params.symbols || {});
  }

  removeToken(glyph) {
    if (this.symbols[glyph]) {
      delete this.symbols[glyph];
    }
    return this;
  }

  addToken (params) {
    var symbol = new Token(params);
    this.symbols[symbol.glyph] = symbol;
    return this;
  }

  hasToken (glyph) {
    return this.symbols.hasOwnProperty(glyph);
  }

  // Rendering is expanding as many times as requested, to a final form.
  resolve (depth) {
    depth = parseInt((typeof depth === 'undefined') ? this.depth : depth);

    if (!depth) {
      return this.axiom;
    }

    var out = this.axiom;
    for (var i = 0; i < depth; i++) {
      out = this.expand(out);
    }
    return out;
  }

  render (depth) {
    var renderString = this.resolve(depth);
    var tokens = this.tokenize(renderString);

    for (var t = 0; t < tokens.length; t++) {
      this.emit("render-token", { token: tokens[t] });
    }

    return renderString;
  }

  tokenize (axiom) {
    axiom = axiom || this.axiom;

    var glyphs = Object.keys(this.symbols);
    // Sort from longest to shortest so we can expand long ones first, in
    // case they contain short ones.
    glyphs.sort((a,b) => (b.length - a.length));

    for (var g = 0; g < glyphs.length; g++) {
      if (glyphs[g] === '+') {
        glyphs[g] = '\\' + glyphs[g];
      }
    }

    var pattern = new RegExp('(' + glyphs.join('|') + ')');
    var tokens = axiom.split(pattern);

    for (var t = 0; t < tokens.length; t++) {
      if (tokens[t] === '') {
        tokens.splice(t--, 1);
      }
    }

    return tokens;
  }

  // Expanding is resolveing one level.
  expand (axiom) {
    axiom = axiom || this.axiom;

    var tokens = this.tokenize(axiom);

    var result = '';
    for (var t = 0; t < tokens.length; t++) {
      if (tokens[t].trim() === '') {
        continue;
      }

      // If we get an invalid symbol, emit an error event.  Let the token
      // pass-through unaltered.
      if (!this.symbols[tokens[t]]) {
        this.emit('[LSystem.expand]error:unknown-token', "unknown token: " + tokens[t]);
        result += tokens[t];
        continue;
      }

      result += this.symbols[tokens[t]].expansion;
    }

    return result;
  }


  get depth () {
    return this._depth;
  }

  set depth (val) {
    this._depth = parseInt((typeof val === 'undefined') ? 1 : val);
  }

  toJSONObject () {
    var obj = {
      axiom: this.axiom,
      depth: this.depth,
      symbols: {}
    };

    for (var glyph in this.symbols) {
      obj.symbols[glyph] = this.symbols[glyph].toJSONObject();
    }

    return obj;
  }

  fromJSONObject (obj) {
    if (typeof obj.axiom   !== "undefined") { this.axiom = obj.axiom; }
    if (typeof obj.depth   !== "undefined") { this.depth = obj.depth; }
    if (typeof obj.symbols !== "undefined") {
      this.symbols = {};
      for (var glyph in obj.symbols) {
        this.addToken(obj.symbols[glyph]);
      }
    }
    return this;
  }

  toJSON () {
    return JSON.stringify(this.toJSONObject());
  }

  fromJSON (str) {
    var json = JSON.parse(str);
    this.fromJSONObject(json);
    return this;
  }

  // Get a new reader.
  getRenderer () {
    var renderer = new Renderer({source: this});
    return renderer;
  }
}

module.exports = LSystem;

