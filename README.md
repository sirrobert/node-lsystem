# README #

A tool for creating and visualizing Lindenmayer systems (L-systems).

## Installation

    npm install sirrobert-lsystem

## Usage Examples

If you already know what L-Systems are, this is probably a good place to
start.  It doesn't get into much theory.  If you would like a primer on
L-Systems first, check out the [Architecture](#Architecture) and
[Theory](## Theory) sections.

Let's get started with some very practical examples.  These assume you've
already imported the module.

### Creating a new lsystem

    var system = new LSystem();

By default, new l-systems start with two symbols: "[" and "]"  These two
symbols are clasically used to save state and restore state to a previously
saved version.  This is generally the core machanism by which "branches" are
created.  For example,




## Architecture

This section assumes you know the general concepts involved in L-Systems.
If you don't, you may want to read the [Theory](#Theory) section.

There are three classes in this module:

* **LSystem**   provides the overarching structure.
* **Token**     provides tokens for grammatical expansion.
* **Renderer**  provides a mechanism for rendering that is output-agnostic.

By separating these concerns, we get a smoother, more malleable and portable
utility from the module.

### LSystem

The LSystem module manages the overall system.  It has the following methods
and properties.

## Theory

I really like practical examples first so that theory has context.  Here's a
practial runthrough that shows the core mechanism of how L-systems work.

## The Mechanism 

LSystems work by creating grammatical statements (called *axioms*) that are
progressively *resolved* into more detailed statements.  This resolution
happens by *expanding* the statement's *symbols*.  Let's look at a simple
example.

### A Basic Example

Let's say the grammar defines that the symbol "A" expands into "BC".  If we
have an axiom "AA", and expand it once, each "A" is replaced with "BC"
and we go from "AA" to "BCBC".  Because we have no rules by which to expand
the symbols "B" or "C", further expansion has no effect:  the axiom 
"BCBC", if expanded again, remains "BCBC".

### Multiple Rules

From here on we will use the notation *A &#8594; BC* to mean *"A" expands
into "BC"*.

If we have multiple rules it works largely the same way.  Let's say we have
these rules:

* A &#8594; BC
* B &#8594; CC

If we start with the same initial axiom, "AA", then expanding it once,
we go from "AA" to "BCBC".  This time, because we have have a rule for how
to expand the symbol "B", we can go from "BCBC" to "CCCCCC".  Note that each
"C" stayed a "C", but each "B" became two "C"s.  That's why we have six
"C"s.

### Simple Recursion

Things get more interesting when we allow for recursive rules.  Imagine a
very simple recursion:

* A &#8594; AA

With this rule, our the axiom "A" will expand once into "AA".  If we
expand it again, we go from "AA" to "AAAA".  The length with continue
doubling because the expansion rule could effectively be read as "Each A
should be doubled."

### Recursion *Alternando*

Let's try a slightly more complex set of rules:

* A &#8594; B
* B &#8594; A

In this system, each "A" is replaced by "B" and each "B" is replaced by "A".
Let's look at an initial axiom of "A" and see how it expands after
successive expansions:

**Axiom: A**

Expansions:

1. B
1. A
1. B
1. A
1. B
1. A
1. ...

Because A &#8594; B and B &#8594; A, expansion of the axiom simply occilates
between the two values.

### More complicated recursion

* A &#8594; ABA
* B &#8594; CAC

Run through, it looks like this:

**Axiom: A**

Expansions:

1. ABA
1. ABACACABA
1. ABACACABACABACABACACABA
1. ABA CAC ABA C ABA C ABA CAC ABA C ABA CAC ABA C ABA CAC ABA C ABA C ABA CAC ABA

I added spaces to the last expansion to make it more intelligible.  You can
see that this gets nice and intricate very quickly.

### How is this useful?

Lindenmayer used this to create grammatical systems that represented the
shapes of plants.  The reason this concept works is because many plants grow
according to a set of organizing principles that cause them to have
*internal self-similarity*.  This means that the plant viewed at a large
scale has structural similarities to parts of the plant viewed at a smaller
scale.

This happens in nature quite frequently and the phenomenon is commonly known
by words like *fractal*.  

So, L-Systems are useful for describing "fractal" structures.  How does this
work?

### Rendering L-Systems

An expanded L-System axiom can be understood as a *well-ordered list of
symbols*.  If this is the case, and if we choose to interpret those symbols
as *instructions*, then we can think of an axiom and its expanded versions
as *lists of well-ordered instructions*.

In the most common case for L-Systems, those instructions refer to drawing.
When they are, they tend to generate realistic flora shapes, interesting
fractal shapes, and more.

**I'd like to put some examples here, but I don't have any at the moment.**

However, there's no real need for these to be interpreted as drawing
instructions.  Some people have used them to generate music (where each
symbol, say, "A" could refer to, say, playing an A-chord for 1 second).

For example, it could be used as a set of instructions for building a
building (in real life or in a game), for manipulating images, or any number
of other domains.

One limitation of this is that the kinds of L-Systems we've looked at are
*deterministic*:  a given axiom *will definitely* expand into a given
statement if the rules and expansion depth are known.  There are, however,
other kinds of rules that allow for greater flexibility in statement
expansion, such as using a probability function to determine whether it will
expand one way or another.  This kind of L-System is called a *stochastic*
L-System.

### Stochastic L-Systems

Stochastic L-Systems are those with rules whose expansions vary according to
probabilities.  For example, if we had the axiom "A" with the following
rules:

* A &#8594; BAB
* B &#8594; C

then our expansion series will look like this for the first five expansions:

*  A

Expansions
1. BAB
1. CBABC
1. CCBABCC
1. CCCBABCCC
1. CCCCBABCCCC

But if we redefine our rules like this (where the six-sided die means a
stochastic rule, and the parenthetical statement shows the probabilities for
each endings).

* A &#9861; (50% AB; 50% BA)
* B &#9861; (50% CA; 50% BC)

Because of the *stochastic* nature of the rules, we could get multiple
different outcomes from the same axiom.

### Recommended Reading

*The Wikipedia Article*

The Wikipedia article on L-Systems is pretty good.

*The Algorithmic Beauty of Plants*

I first got introduced to L-Systems through the book "The Algorithmic Beauty
of Plants" in the 1990s when doing some 3D modeling work with POV-Ray.  It's
a great place to start if you want a solid intro.

