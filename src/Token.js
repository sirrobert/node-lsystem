"use strict";

const EventEmitter = require('events');

class Token extends EventEmitter {
  constructor (params) {
    super();
    params = params || {};

    if (typeof params.glyph !== 'undefined') {
      this.glyph = params.glyph;
      this.expansion = params.glyph;
    }

    if (typeof params.expansion !== 'undefined') {
      this.expansion = params.expansion;
    }

    if (params.isTerminal === true) {
      this.expansion = this.glyph;
    }
  }

  get glyph () {
    return this._glyph;
  }

  set glyph (val) {
    this._glyph = '' + val;
  }

  get expansion () {
    return this._expansion;
  }

  set expansion (val) {
    this._expansion = val;
  }

  get isVariable () {
    return !this.isTerminal;
  }

  set isTerminal (val) {
    if (val) {
      this.expansion = this.glyph;
    }
  }

  get isTerminal () {
    if (this.glyph === this.expansion) {
      return true;
    }
    return false;
  }

  toJSONObject () {
    return {
      glyph      : this.glyph,
      expansion  : this.expansion,
      isTerminal : this.isTerminal,
    };
  }

}

module.exports = Token;
