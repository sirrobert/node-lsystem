"use strict";

const EventEmitter = require('events');
const Token       = require('./Token.js');
const Stack        = require('sirrobert-stack');

class Renderer extends EventEmitter {

  constructor (params) {
    super();
    params = params || {};

    this.source = null;
    this.tokenActions = {};

    if (params.source) {
      this.source = params.source;
    }

    if (params.onToken) {
      for (var token in params.onToken) {
        this.addTokenAction(token, params.onToken[token]);
      }
    }

    // In general, state is a stack.
    this.state = new Stack();
  }
  
  removeTokenAction (token) {
    delete this.tokenAction[token];
  }

  addTokenAction (token, action) {
    this.tokenActions[token] = action;
  }

  get source () {
    return this._source;
  }

  set source (val) {
    if (val && (val instanceof EventEmitter)) {
      this._source = val;
      this._source.on("render-token", (data) => { this.handleRenderEvent(data) });
    } 
  }

  // The data here should loook like this:
  handleRenderEvent (data) {
    if (data && data.token && this.tokenActions[data.token]) {
      this.tokenActions[data.token](data);
    }
  }

}

module.exports = Renderer;

